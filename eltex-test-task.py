import random

str_chars = 'abcdefghijklmnopqrstuvwxyz'
str_nums = '0123456789'
str_res = str_chars + str_nums
new_str = ""

# 1 ------------------

while True:
	num = input("Enter num: ")
	try:
		num = int(num)
	except ValueError: print("Error. It's not a number.")
	else: break

for i in range(num):
	ch_n = random.randint(0, len(str_res) - 1)
	upper_flag = random.randint(0, 1)
	ch = str_res[ch_n]
	if not upper_flag:
		ch = ch.upper()
	new_str += ch

print(new_str)

# 2 ------------------

while True:
	ch_1 = str(input("Enter first char: "))
	if len(ch_1) == 1: break
	else: print("Error input")

for i in range(num):
	if new_str[i].lower() in str_chars:
		new_str = new_str[:i] + ch_1 + new_str[i+1:]

# print(new_str)

# 3 ------------------
# для оптимизации шаги 2, 3 и 4
# можно объединить в один цикл for
# но я четко следовал тз

while True:
	ch_2 = str(input("Enter second char: "))
	if len(ch_2) == 1: break
	else: print("Error input")

for i in range(num):
	if new_str[i] != ch_1:
		new_str = new_str[:i] + ch_2 + new_str[i+1:]

print(new_str)

# 4 ------------------

print("Count of first char:", new_str.count(ch_1))

print("Count of second char:", new_str.count(ch_2))